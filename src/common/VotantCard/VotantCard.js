import React from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import {VotantCardStyled} from './style';
import CircularProgress from '@material-ui/core/CircularProgress';

export class VotantCard extends React.Component {
  render() {
    return (
      <VotantCardStyled>
        {({classes}) => (
          <div className={classes.container}>
            <p className={classes.cardHeader}>
              Username: <strong>{this.props.username}</strong>
            </p>
            <Card className={classes.card} raised={true}>
              <CardContent>
                {this.props.voting ? (
                  <div
                    style={{
                      display: 'flex',
                      alignItems: 'center',
                      flexDirection: 'column',
                    }}>
                    <p style={{textAlign: 'center'}}>
                      Waiting for users to vote
                    </p>
                    <CircularProgress />
                  </div>
                ) : (
                  <p className={classes.voteNumber}>{this.props.vote}</p>
                )}
              </CardContent>
            </Card>
          </div>
        )}
      </VotantCardStyled>
    );
  }
}
