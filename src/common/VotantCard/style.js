import {createStyled} from '../MaterialUi/createStyled';

export const VotantCardStyled = createStyled({
  container: {
    alignItems: 'center', display: 'flex', flexDirection: 'column', justifyContent: 'center', marginBottom: 25,
  }, cardHeader: {
    display: 'flex', justifyContent: 'space-between', paddingBottom: 10,
  }, card: {
    alignItems: 'center', background: 'white', border: '1px solid #1B3A90', borderRadius: 10,
    boxShadow: '10px 10px 10px #1B80BFBF', display: 'flex', justifyContent: 'center', minHeight: 320, width: '75%',
  }, voteNumber: {
    fontSize: 125, fontWeight: 600,
  },
});
