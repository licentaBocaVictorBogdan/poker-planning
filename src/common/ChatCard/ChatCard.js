import React from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import {ChatCardStyled} from './style';
import CircularProgress from '@material-ui/core/CircularProgress';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

export class ChatCard extends React.Component {
  state = {
    cardContent: this.props.cardContent ? this.props.cardContent : '',
  };

  handleInputInteraction = event =>
    this.setState({cardContent: event.target.value});
  handleSubmit = event => {
    event.preventDefault();

    this.props.handleSubmit(this.state.cardContent);
  };

  render() {
    const center = this.props.needToArgument ? false : !this.props.cardContent;
    return (
      <ChatCardStyled>
        {({classes}) => (
          <div className={classes.container}>
            <p className={classes.cardHeader}>{this.props.cardTitle}</p>
            <Card
              className={`${classes.card} ${center && classes.centerContainer}`}
              raised={true}>
              <CardContent>
                {this.props.needToArgument && !this.props.cardContent ? (
                  <form
                    onSubmit={this.handleSubmit}
                    className={classes.cardForm}>
                    <TextField
                      placeholder="Please add your argument"
                      className={classes.cardContent}
                      multiline={true}
                      rowsMax={15}
                      autoFocus={true}
                      onChange={this.handleInputInteraction}
                      value={this.state.cardContent}
                    />
                    <Button type="submit"> Send</Button>
                  </form>
                ) : this.props.cardContent ? (
                  <TextField
                    disabled={true}
                    className={classes.cardContent}
                    multiline={true}
                    rowsMax={15}
                    defaultValue={this.props.cardContent}
                  />
                ) : (
                  <>
                    <p>Waiting for user to argument</p>
                    <CircularProgress />
                  </>
                )}
              </CardContent>
            </Card>
          </div>
        )}
      </ChatCardStyled>
    );
  }
}
