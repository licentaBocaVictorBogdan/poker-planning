import {createStyled} from '../MaterialUi/createStyled';

export const ChatCardStyled = createStyled({
  container: {
    display: 'flex', flexDirection: 'column', margin: 25,
  }, centerContainer: {
    alignItems: 'center', display: 'flex', height: '100%', justifyContent: 'center',
  }, cardTitle: {
    display: 'flex', paddingBottom: 10, width: '75%',
  }, card: {
    background: 'white', border: '1px solid #1B3A90', borderRadius: 10, boxShadow: '10px 10px 10px #1B80BFBF',
    minHeight: 320, width: '100%',
  }, cardContent: {
    height: '100%', textIndent: 15, width: '100%',
  }, cardForm: {
    display: 'flex',
  },
});
