import * as React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import {HoursVoteStyled} from './style';
import {PPInput} from '../../style/inputs';
import FormHelperText from '@material-ui/core/FormHelperText';

export class HoursVote extends React.PureComponent {
  state = {
    error: false,
    personsVote: this.props.personsVote ? this.props.personsVote : '',
  };

  componentWillReceiveProps(nextProps) {
    this.setState({
      personsVote: nextProps.personsVote ? nextProps.personsVote : '',
    });
  }

  handleInputInteraction = event => {
    this.props.onChange && this.props.onChange(event.target.value);
    this.setState({
      personsVote: event.target.value,
      error: false,
    });
  };
  onIncrease = () => {
    this.props.onChange && this.props.onChange(this.state.personsVote + 1);
    this.setState({
      personsVote:
        parseInt(this.state.personsVote ? this.state.personsVote : 0) + 1,
      error: false,
    });
  };
  onDecrease = () => {
    this.props.onChange && this.props.onChange(this.state.personsVote - 1);
    this.setState({
      personsVote:
        parseInt(this.state.personsVote ? this.state.personsVote : 0) - 1,
      error: false,
    });
  };

  onSave = event => {
    event.preventDefault();

    if (this.state.personsVote < 2) {
      this.setState({error: true});
    } else {
      this.props.onSave(this.state.personsVote);
    }
  };

  render() {
    return (
      <HoursVoteStyled>
        {({classes}) => (
          <form className={classes.root} onSubmit={this.onSave}>
            <div>
              <Button
                onClick={this.onDecrease}
                color="secondary"
                disabled={this.props.disabled}
                className={classes.button}>
                {' '}
                -{' '}
              </Button>
              <PPInput>
                {({classes: PPIclass}) => (
                  <TextField
                    label={'Hours vote'}
                    variant="outlined"
                    type="number"
                    disabled={this.props.disabled}
                    error={this.state.error}
                    inputProps={{min: 2}}
                    required={true}
                    className={`${PPIclass.input} ${classes.input}`}
                    value={this.state.personsVote}
                    onChange={this.handleInputInteraction}
                  />
                )}
              </PPInput>
              {this.state.error && (
                <FormHelperText error={this.state.error}>
                  Min value: 2
                </FormHelperText>
              )}
              <Button
                onClick={this.onIncrease}
                color="secondary"
                disabled={this.props.disabled}
                className={classes.button}>
                {' '}
                +{' '}
              </Button>
            </div>
            <Button
              onClick={this.onSave}
              color="secondary"
              disabled={this.props.disabled}
              type="submit">
              {' '}
              {this.props.buttonText}{' '}
            </Button>
          </form>
        )}
      </HoursVoteStyled>
    );
  }
}
