import {createStyled} from '../MaterialUi/createStyled';

export const HoursVoteStyled = createStyled({
  root: {
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
    justifyContent: 'space-around',
    margin: 25,
  },
  input: {
    width: '100%',
  },
  button: {
    margin: '15px 0',
    width: '100%',
  },
});
