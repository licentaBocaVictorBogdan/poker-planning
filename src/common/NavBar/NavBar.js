import AppBar from '@material-ui/core/AppBar/AppBar';
import Toolbar from '@material-ui/core/Toolbar/Toolbar';
import * as React from 'react';
import {NavBarStyled} from './style';
import Grid from '@material-ui/core/Grid';

export class NavBar extends React.Component {
  render() {
    return (
      <NavBarStyled>
        {({classes}) => (
          <AppBar position="static" className={classes.root}>
            <Toolbar>
              <Grid container={true}>
                <Grid item={true} xs={12} md={6}>
                  {this.props.leftSide}
                </Grid>
                <Grid item={true} xs={12} md={6}>
                  {this.props.rightSide}
                </Grid>
              </Grid>
            </Toolbar>
          </AppBar>
        )}
      </NavBarStyled>
    );
  }
}
