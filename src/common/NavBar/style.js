import {createStyled} from '../MaterialUi/createStyled';

export const NavBarStyled = createStyled({
  root: {
    backgroundColor: '#8BCDBC',
  },
});
