import {createStyled} from '../MaterialUi/createStyled';

export const MakRoomStyled = createStyled({
  form: {
    display: 'flex',
    flexDirection: 'column',
    marginRight: 75,
  },
  title: {
    fontSize: 36,
    marginLeft: 10,
    marginTop: 40,
  },
  input: {
    margin: 10,
  },
  checkbox: {
    marginLeft: 10,
    '& span': {
      fontWeight: 500,
      paddingLeft: 0,
    },
  },
  button: {
    height: 36,
    margin: '30px 10px 10px 10px',
  },
});
