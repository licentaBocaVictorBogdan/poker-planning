import * as React from 'react';
import FormControlLabel from '@material-ui/core/FormControlLabel/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox/Checkbox';
import TextField from '@material-ui/core/TextField/TextField';
import {PPInput} from '../../style/inputs';
import {MakRoomStyled} from './style';
import Button from '@material-ui/core/Button/Button';

const initialState = {
  allPresent: false,
  persons: '',
  name: '',
};

export class MakeRoomForm extends React.Component {
  state = {...initialState};

  handleCheckboxInteraction = () =>
    this.setState({allPresent: !this.state.allPresent});
  handleSubmit = event => {
    this.props.onSubmit(this.state);

    this.resetForm();
    event.preventDefault();
  };
  handleInputChange = name => event =>
    this.setState({[name]: event.target.value});
  resetForm = () => this.setState({...initialState});

  render() {
    return (
      <MakRoomStyled>
        {({classes}) => (
          <PPInput>
            {({classes: inputClasses}) => (
              <form onSubmit={this.handleSubmit} className={classes.form}>
                <h1 className={classes.title}> Create a Room</h1>
                <TextField
                  label="Task name"
                  className={[inputClasses.input, classes.input].join(' ')}
                  value={this.state.name}
                  onChange={this.handleInputChange('name')}
                  margin="normal"
                  variant="outlined"
                  required={true}
                />
                <TextField
                  label="Number of persons"
                  className={[inputClasses.input, classes.input].join(' ')}
                  value={this.state.persons}
                  type="number"
                  onChange={this.handleInputChange('persons')}
                  margin="normal"
                  variant="outlined"
                  required={true}
                />
                <FormControlLabel
                  className={classes.checkbox}
                  control={
                    <Checkbox
                      checked={this.state.allPresent}
                      onChange={this.handleCheckboxInteraction}
                      value="allPresent"
                      color="primary"
                    />
                  }
                  label="All persons present"
                />
                <Button
                  color="secondary"
                  type="submit"
                  className={classes.button}>
                  {' '}
                  Create a Room
                </Button>
              </form>
            )}
          </PPInput>
        )}
      </MakRoomStyled>
    );
  }
}
