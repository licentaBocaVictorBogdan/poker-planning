import * as React from 'react';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import IconButton from '@material-ui/core/IconButton';
import Collapse from '@material-ui/core/Collapse';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Divider from '@material-ui/core/Divider';

export class RoomCard extends React.Component {
  state = {expanded: false};

  handleExpandClick = () => {
    this.setState(state => ({expanded: !state.expanded}));
  };

  render() {
    const title = `Room name:${this.props.title}`;
    const subtitle = `RoomId:${this.props.roomId}`;

    return (
      <Card style={{background: '#f0f0f0'}}>
        <CardHeader
          title={title}
          subheader={subtitle}
          action={
            <IconButton
              onClick={this.handleExpandClick}
              aria-expanded={this.state.expanded}>
              <ExpandMoreIcon />
            </IconButton>
          }
        />
        <Collapse in={this.state.expanded} timeout="auto">
          <Divider />
          <CardContent>
            <Typography paragraph>
              Description: {this.props.description}
            </Typography>
            <Typography paragraph>
              Final vote:{' '}
              {this.props.hours ? `${this.props.hours} hours` : 'Not voted yet'}
            </Typography>
          </CardContent>
        </Collapse>
      </Card>
    );
  }
}
