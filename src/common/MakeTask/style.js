import {createStyled} from '../MaterialUi/createStyled';

export const MakeTaskStyled = createStyled({
  form: {
    display: 'flex', flex: 1, flexDirection: 'column',
  }, title: {
    fontSize: 36, marginLeft: 10, marginTop: 40,
  }, inputsContainer: {
    display: 'flex', justifyContent: 'space-between',
  }, input: {
    flex: 1, margin: '5px 0 5px 10px !important',
  }, textarea: {
    flex: 1, margin: '5px 0 5px 10px !important', maxHeight: 200, minHeight: 60,
  }, submitButton: {
    margin: '30px 10px 10px 10px',
  },
});
