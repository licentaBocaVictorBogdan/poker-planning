import * as React from 'react';
import {MakeTaskStyled} from './style';
import TextField from '@material-ui/core/TextField/TextField';
import {PPInput} from '../../style/inputs';
import Button from '@material-ui/core/Button/Button';
import {axiosInstance} from '../../config';
import {RoomIdDialog} from './RoomIdDialog';

const initialState = {
  name: '',
  description: '',
  persons: '',
};

export class MakeTask extends React.Component {
  constructor(props) {
    super(props);
    this.state = {...initialState};
  }

  handleChange = name => event => this.setState({[name]: event.target.value});
  handleSubmit = event => {
    event.preventDefault();

    const {roomId, ...rest} = this.state;

    axiosInstance.post('/room', {...rest}).then(({data}) => {
      this.resetInputs();
      this.setState({roomId: data.roomId});
    });
  };
  resetInputs = () => this.setState({...initialState});

  render() {
    return (
      <MakeTaskStyled>
        {({classes}) => (
          <PPInput>
            {({classes: inputClasses}) => (
              <form className={classes.form} onSubmit={this.handleSubmit}>
                <h1 className={classes.title}>Make a task for voting</h1>
                <TextField
                  className={[inputClasses.input, classes.input].join(' ')}
                  label="Task Name"
                  value={this.state.name}
                  onChange={this.handleChange('name')}
                  margin="normal"
                  variant="outlined"
                  required={true}
                />
                <TextField
                  className={[inputClasses.input, classes.textarea].join(' ')}
                  label="Task Description"
                  multiline={true}
                  rowsMax={5}
                  value={this.state.description}
                  onChange={this.handleChange('description')}
                  margin="normal"
                  variant="outlined"
                  required={true}
                />
                <TextField
                  className={[inputClasses.input, classes.input].join(' ')}
                  label="Number of persons"
                  value={this.state.persons}
                  onChange={this.handleChange('persons')}
                  margin="normal"
                  variant="outlined"
                  required={true}
                  inputProps={{min: 2}}
                  type={'number'}
                />

                <Button className={classes.submitButton} type="submit">
                  Create task
                </Button>
                {this.state.roomId && (
                  <RoomIdDialog roomId={this.state.roomId} />
                )}
              </form>
            )}
          </PPInput>
        )}
      </MakeTaskStyled>
    );
  }
}
