import * as React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogActions from '@material-ui/core/DialogActions';

export class RoomIdDialog extends React.Component {
  state = {
    open: true,
  };
  handleClose = () => this.setState({open: false});

  render() {
    return (
      <Dialog
        fullScreen={window.screen.width < 960}
        fullWidth={true}
        maxWidth="sm"
        open={this.state.open}
        onClose={this.handleClose}>
        <DialogTitle id="alert-dialog-title">
          {'Please join to this room id'}
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Room Id: <strong>{this.props.roomId}</strong>
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={this.handleClose} color="primary">
            Close
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}
