import * as React from 'react';
import * as PPImage from '../../style/img/285.jpg';
import {InformationsStyled} from './style';

export class Informations extends React.Component {
  render() {
    return (
      <InformationsStyled>
        {({classes}) => (
          <div className={classes.container}>
            <p className={classes.text}>
              Poker Planning is the funniest and easiest way for your team to
              effectively plan your sprint planning sessions.Poker Planning is
              the secure, fun way for agile teams to guide sprint planning and
              build accurate consensus estimates.
            </p>
            <p className={classes.text}>
              Planning poker is a consensus-based, gamified technique for
              estimating, mostly used to estimate effort or relative size of
              development goals in software development.
            </p>
            <img src={PPImage} alt="planning" className={classes.image} />
          </div>
        )}
      </InformationsStyled>
    );
  }
}
