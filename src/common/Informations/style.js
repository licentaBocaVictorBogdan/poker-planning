import {createStyled} from '../MaterialUi/createStyled';

export const InformationsStyled = createStyled({
  container: {
    alignItems: 'center',
    display: 'flex',
    flex: 1,
    flexDirection: 'column',
    margin: 25,
  },
  text: {
    textIndent: 15,
  },
  image: {
    marginTop: 50,
    width: 300,
  },
});
