import {createStyled} from '../MaterialUi/createStyled';

export const LoginFromStyled = createStyled({
  container: {
    alignItems: 'center',
    display: 'flex',
  },
  inputsContainer: {
    display: 'flex',
    justifyContent: 'center',
  },
  button: {
    justifyContent: 'center',
    height: 36,
    margin: '0 45px 0 90px ',
    width: 155,
  },
});
