import * as React from 'react';
import Button from '@material-ui/core/Button/Button';
import TextField from '@material-ui/core/TextField/TextField';
import {LoginFromStyled} from './style';
import {PPInput} from '../../style/inputs';
import InputAdornment from '@material-ui/core/InputAdornment/InputAdornment';
import IconButton from '@material-ui/core/IconButton/IconButton';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import Grid from '@material-ui/core/Grid';

export class LoginForm extends React.Component {
  state = {
    username: '',
    password: '',
    showPassword: false,
  };

  handleSubmit = event => {
    const {showPassword, ...rest} = this.state;
    this.props.onLoginSubmit(rest);
    event.preventDefault();
  };

  handleInputInteraction = event =>
    this.setState({[event.target.name]: event.target.value});
  handleClickShowPassword = () =>
    this.setState({showPassword: !this.state.showPassword});

  render() {
    const {error} = this.props;
    return (
      <LoginFromStyled>
        {({classes}) => (
          <form className={classes.container} onSubmit={this.handleSubmit}>
            <Grid container={true} spacing={32}>
              <PPInput>
                {({classes: inputClasses}) => (
                  <>
                    <Grid item={true} xs={12} md={1} />
                    <Grid item={true} xs={12} md={3}>
                      <TextField
                        autoFocus={true}
                        error={error}
                        label="Username"
                        type="text"
                        className={[classes.input, inputClasses.input].join(
                          ' ',
                        )}
                        margin="normal"
                        name="username"
                        onChange={this.handleInputInteraction}
                        required={true}
                        variant="outlined"
                        fullWidth={true}
                        inputProps={{minLength: 4}}
                      />
                    </Grid>
                    <Grid item={true} xs={12} md={3}>
                      <TextField
                        label="Password"
                        error={error}
                        type={this.state.showPassword ? 'text' : 'password'}
                        className={[classes.input, inputClasses.input].join(
                          ' ',
                        )}
                        margin="normal"
                        name="password"
                        onChange={this.handleInputInteraction}
                        required={true}
                        variant="outlined"
                        fullWidth={true}
                        InputProps={{
                          endAdornment: (
                            <InputAdornment position="end">
                              <IconButton
                                aria-label="Toggle password visibility"
                                onClick={this.handleClickShowPassword}>
                                {this.state.showPassword ? (
                                  <VisibilityOff />
                                ) : (
                                  <Visibility />
                                )}
                              </IconButton>
                            </InputAdornment>
                          ),
                        }}
                      />
                    </Grid>
                    <Grid item={true} xs={12} md={1} />
                  </>
                )}
              </PPInput>
              <Grid item={true} xs={12} md={3}>
                <Button color="secondary" type="submit" fullWidth={true}>
                  Login
                </Button>
              </Grid>
              <Grid item={true} xs={12} md={1} />
            </Grid>
          </form>
        )}
      </LoginFromStyled>
    );
  }
}
