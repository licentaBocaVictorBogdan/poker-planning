import * as React from 'react';
import TextField from '@material-ui/core/TextField/TextField';
import {PPInput} from '../../style/inputs';
import {MakeAccountStyled} from './style';
import Button from '@material-ui/core/Button/Button';

const initialState = {
  companyName: '',
  email: '',
  username: '',
  password: '',
  passwordAgain: '',
  showPassword: false,
};

export class MakeAccountForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {...initialState};
  }

  handleChange = name => event => this.setState({[name]: event.target.value});
  handleSubmit = event => {
    const {showPassword, ...rest} = this.state;
    this.props.onFormSave(rest);
    event.preventDefault();
  };

  render() {
    return (
      <MakeAccountStyled>
        {({classes}) => (
          <PPInput>
            {({classes: inputClasses}) => (
              <form onSubmit={this.handleSubmit} className={classes.form}>
                <h1 className={classes.title}>Make an account</h1>
                <TextField
                  className={[inputClasses.input, classes.input].join(' ')}
                  label="Username"
                  value={this.state.username}
                  onChange={this.handleChange('username')}
                  margin="normal"
                  variant="outlined"
                  required={true}
                  inputProps={{minLength: 4}}
                  error={this.props.userOrEmailError}
                />
                <TextField
                  className={[inputClasses.input, classes.input].join(' ')}
                  label="Email"
                  value={this.state.email}
                  onChange={this.handleChange('email')}
                  margin="normal"
                  variant="outlined"
                  required={true}
                  type={'email'}
                  error={this.props.userOrEmailError}
                />
                <TextField
                  className={[inputClasses.input, classes.input].join(' ')}
                  label="Password"
                  value={this.state.password}
                  type="password"
                  onChange={this.handleChange('password')}
                  margin="normal"
                  variant="outlined"
                  required={true}
                  inputProps={{minLength: 6}}
                  error={this.props.passwordError}
                />
                <TextField
                  className={[inputClasses.input, classes.input].join(' ')}
                  label="Password again"
                  value={this.state.passwordAgain}
                  type="password"
                  onChange={this.handleChange('passwordAgain')}
                  margin="normal"
                  variant="outlined"
                  required={true}
                  inputProps={{minLength: 6}}
                  error={this.props.passwordError}
                />
                <TextField
                  className={[inputClasses.input, classes.input].join(' ')}
                  label="Company name"
                  value={this.state.companyName}
                  onChange={this.handleChange('companyName')}
                  margin="normal"
                  variant="outlined"
                />

                <p className={classes.errorMessage}>
                  <strong>{this.props.errorMessage}</strong>
                </p>

                <Button
                  color="secondary"
                  type="submit"
                  className={classes.submitButton}>
                  Make an account
                </Button>
              </form>
            )}
          </PPInput>
        )}
      </MakeAccountStyled>
    );
  }
}
