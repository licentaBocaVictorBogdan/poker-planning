import {createStyled} from '../MaterialUi/createStyled';

export const MakeAccountStyled = createStyled({
  form: {
    display: 'flex',
    flex: 1,
    flexDirection: 'column',
    marginLeft: 25,
    marginRight: 25,
  },
  title: {
    fontSize: 36,
    marginLeft: 10,
    marginTop: 40,
  },
  inputsContainer: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  input: {
    flex: 1,
    margin: 10,
  },
  submitButton: {
    margin: '30px 10px 10px 10px',
  },
  errorMessage: {
    fontSize: 16,
    color: '#FF1111',
    transition: 'color 2s ease',
    padding: 14,
  },
});
