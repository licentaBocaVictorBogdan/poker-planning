import Paper from '@material-ui/core/Paper/Paper';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';
import * as React from 'react';
import {hot} from 'react-hot-loader';
import {BrowserRouter as Router} from 'react-router-dom';
import {theme} from './theme';
import {AuthButton} from './routes/MainRoute';

class App extends React.Component {
  render() {
    return (
      <MuiThemeProvider theme={theme}>
        <Router>
          <Paper
            style={{
              height: '100%',
              overflow: 'auto',
            }}>
            <AuthButton />
          </Paper>
        </Router>
      </MuiThemeProvider>
    );
  }
}

export default hot(module)(App);
