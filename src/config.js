const axios = require('axios');
const baseURL = 'http://192.168.100.7:8080/';

export const localStorageTokenName = 'pokerPlanningToken';

export const axiosInstance = axios.create({
  baseURL,
});

function getLSObject() {
  return JSON.parse(
    atob(
      JSON.parse(localStorage.getItem(localStorageTokenName)).token.split(
        '.',
      )[1],
    ),
  );
}

export function getUserId() {
  return getLSObject().userId;
}

export function getUsername() {
  return getLSObject().username;
}
