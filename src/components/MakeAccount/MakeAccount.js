import * as React from 'react';
import {MakeAccountForm} from '../../common/MakeAccountForm/MakeAccountForm';
import {axiosInstance} from '../../config';
import {withRouter} from 'react-router-dom';
import {checkAuth} from '../../routes/checkAuth';

const initialState = {
  passwordError: false,
  userOrEmailError: false,
  errorMessage: '',
};

class MakeAccount extends React.Component {
  state = {...initialState};

  saveUser = ({companyName, username, email, password, passwordAgain}) => {
    if (password !== passwordAgain) {
      this.setState({
        passwordError: true,
        errorMessage: "Password doesn't match",
      });
    } else {
      axiosInstance
        .post('signup', {
          companyName,
          username,
          email,
          password,
          passwordAgain,
        })
        .then(() => {
          axiosInstance
            .post('login', {
              username,
              password,
            })
            .then(({data}) => {
              axiosInstance.defaults.headers.common[
                'Authorization'
              ] = `Bearer ${data.token}`;
              checkAuth.authenticate(data, () =>
                this.props.history.push('/home'),
              );
            });
        })
        .catch(() =>
          this.setState({
            userOrEmailError: true,
            errorMessage:
              'An account with same username or email already exist.',
          }),
        );
    }
  };

  render() {
    const {passwordError, userOrEmailError, errorMessage} = this.state;
    return (
      <MakeAccountForm
        onFormSave={this.saveUser}
        passwordError={passwordError}
        userOrEmailError={userOrEmailError}
        errorMessage={errorMessage}
      />
    );
  }
}

export default withRouter(MakeAccount);
