import * as React from 'react';
import {MakeRoomForm} from '../../common/MakeRoomForm/MakeRoomForm';
import {axiosInstance} from '../../config';
import {Redirect} from 'react-router-dom';

export class MakeRoom extends React.Component {
  state = {
    redirect: false,
  };

  handleSubmit = ({name, persons, allPresent}) => {
    return axiosInstance
      .post('/room', {
        name,
        persons,
        allPresent,
      })
      .then(({data}) => {
        this.setState({
          redirect: (
            <Redirect
              to={{
                pathname: '/access',
                state: {roomId: data.roomId},
              }}
            />
          ),
        });
      });
  };

  render() {
    return this.state.redirect ? (
      this.state.redirect
    ) : (
      <MakeRoomForm onSubmit={this.handleSubmit} />
    );
  }
}
