import {createStyled} from '../../common/MaterialUi/createStyled';

export const ChangePasswordStyled = createStyled({
  root: {
    minWidth: 750,
  }, container: {
    marginTop: 25, width: '100%',
  }, input: {
    margin: 15, width: '100%',
  }, button: {
    padding: '0 15px', margin: '0 15px',
  },
});
