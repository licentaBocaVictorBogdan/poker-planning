import * as React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import Button from '@material-ui/core/Button';
import DialogTitle from '@material-ui/core/DialogTitle';
import {PPInput} from '../../style/inputs';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import {ChangePasswordStyled} from './style';
import DialogActions from '@material-ui/core/DialogActions';

export class ChangePassword extends React.Component {
  state = {
    open: this.props.open,
    oldPassword: '',
    newPassword: '',
    newPasswordAgain: '',
    error: true,
  };

  componentWillReceiveProps(nextProps, nextContext) {
    this.setState({
      open: nextProps.open,
      error: nextProps.error ? nextProps.error : false,
    });
  }

  handleClose = () => {
    this.setState({open: false});
    this.props.onClose();
  };
  handleInputInteraction = event =>
    this.setState({
      [event.target.name]: event.target.value,
      error: false,
    });
  changePassword = () => {
    if (this.state.newPassword !== this.state.newPasswordAgain) {
      this.setState({error: true});
    } else {
      this.props.onSubmit(this.state.oldPassword, this.state.newPassword);
    }
  };

  render() {
    return (
      <ChangePasswordStyled>
        {({classes}) => (
          <Dialog
            open={this.state.open}
            onClose={this.handleClose}
            maxWidth="sm"
            fullWidth={true}
            fullScreen={window.screen.width < 960}>
            <DialogTitle id="alert-dialog-title">
              {'Change password'}
            </DialogTitle>
            <DialogContent>
              <Grid container={true} spacing={40}>
                <form
                  onSubmit={this.changePassword}
                  className={classes.container}>
                  <PPInput>
                    {({classes: inputClasses}) => (
                      <>
                        <Grid item={true} xs={11} md={11}>
                          <TextField
                            autoFocus={true}
                            label="Current password"
                            error={this.props.error}
                            type="password"
                            className={[classes.input, inputClasses.input].join(
                              ' ',
                            )}
                            margin="normal"
                            name="oldPassword"
                            onChange={this.handleInputInteraction}
                            required={true}
                            variant="outlined"
                          />
                        </Grid>
                        <Grid item={true} xs={11} md={11}>
                          <TextField
                            label="New password"
                            type="password"
                            error={this.state.error}
                            className={[classes.input, inputClasses.input].join(
                              ' ',
                            )}
                            margin="normal"
                            name="newPassword"
                            onChange={this.handleInputInteraction}
                            required={true}
                            variant="outlined"
                          />
                        </Grid>
                        <Grid item={true} xs={11} md={11}>
                          <TextField
                            label="Repeat new password"
                            type="password"
                            error={this.state.error}
                            className={[classes.input, inputClasses.input].join(
                              ' ',
                            )}
                            margin="normal"
                            name="newPasswordAgain"
                            onChange={this.handleInputInteraction}
                            required={true}
                            variant="outlined"
                          />
                        </Grid>
                      </>
                    )}
                  </PPInput>
                </form>
              </Grid>
            </DialogContent>
            <DialogActions>
              <Button onClick={this.changePassword} color="secondary">
                Change password
              </Button>
              <Button onClick={this.handleClose} color="primary">
                Cancel
              </Button>
            </DialogActions>
          </Dialog>
        )}
      </ChangePasswordStyled>
    );
  }
}
