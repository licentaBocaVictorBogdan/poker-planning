import * as React from 'react';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import MenuItem from '@material-ui/core/MenuItem';
import {axiosInstance} from '../../config';
import Menu from '@material-ui/core/Menu';
import {ChangePassword} from '../ChangePassword/ChangePassword';
import {checkAuth} from '../../routes/checkAuth';
import {NavBarStyled} from '../../common/NavBar/style';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import {withRouter} from 'react-router-dom';

class LoggedNavBar extends React.Component {
  state = {
    anchorEl: null,
    changePassword: false,
    changePasswordError: false,
  };

  handleClick = event => {
    this.setState({anchorEl: event.currentTarget});
  };

  handleClose = () => {
    this.setState({anchorEl: null});
  };

  changePassword = (currentPassword, newPassword) => {
    axiosInstance
      .post('/changePassword', {currentPassword, newPassword})
      .then(
        () => this.closeDialog(),
        () => this.setState({changePasswordError: true}),
      );
  };
  openDialog = () => this.setState({changePassword: true});
  closeDialog = () => this.setState({changePassword: false});

  redirectRooms = () => this.props.history.push('/rooms');
  redirectHome = () => this.props.history.push('/home');

  logout = () => this.props.history.push('/');

  render() {
    const {anchorEl} = this.state;
    const rightSide = (
      <Grid container={true}>
        <Grid item={true} xs={6} md={10} />
        <Grid item={true} xs={6} md={2}>
          <Button
            aria-owns={anchorEl ? 'simple-menu' : undefined}
            aria-haspopup="true"
            onClick={this.handleClick}>
            Options
          </Button>
          <Menu
            anchorEl={anchorEl}
            open={Boolean(anchorEl)}
            onClose={this.handleClose}>
            <MenuItem onClick={this.redirectHome}>Home</MenuItem>
            <MenuItem onClick={this.redirectRooms}>My rooms</MenuItem>
            <MenuItem onClick={this.openDialog}>Change password</MenuItem>
            <MenuItem onClick={() => checkAuth.signout(this.logout)}>
              Logout
            </MenuItem>
          </Menu>
          <ChangePassword
            open={this.state.changePassword}
            onSubmit={this.changePassword}
            onClose={this.closeDialog}
            error={this.state.changePasswordError}
          />
        </Grid>
      </Grid>
    );

    return (
      <NavBarStyled>
        {({classes}) => (
          <AppBar position="static" className={classes.root}>
            <Toolbar>
              <Grid container={true}>
                <Grid item={true} xs={6} md={6}>
                  <p>{this.props.leftString}</p>
                </Grid>
                <Grid item={true} xs={6} md={6}>
                  {rightSide}
                </Grid>
              </Grid>
            </Toolbar>
          </AppBar>
        )}
      </NavBarStyled>
    );
  }
}

export default withRouter(LoggedNavBar);
