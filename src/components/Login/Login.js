import * as React from 'react';
import {LoginForm} from '../../common/LoginForm/LoginForm';
import {axiosInstance} from '../../config';
import {Redirect, withRouter} from 'react-router-dom';
import {checkAuth} from '../../routes/checkAuth';

class Login extends React.Component {
  state = {
    loginError: false,
    loggedIn: false,
  };

  handleLoginSubmit = ({username, password}) => {
    axiosInstance.post('login', {username, password}).then(
      ({data}) => {
        axiosInstance.defaults.headers.common['Authorization'] = `Bearer ${
          data.token
        }`;
        checkAuth.authenticate(data, () => this.setState({loggedIn: true}));
      },
      () => this.setState({loginError: true}),
    );
  };

  render() {
    return this.state.loggedIn ? (
      <Redirect
        to={
          this.props.location.pathname !== '/'
            ? this.props.location.pathname
            : '/home'
        }
      />
    ) : (
      <LoginForm
        error={this.state.loginError}
        onLoginSubmit={this.handleLoginSubmit}
      />
    );
  }
}

export default withRouter(Login);
