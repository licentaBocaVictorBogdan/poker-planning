import * as React from 'react';
import {AccessRoomStyled} from './style';
import Redirect from 'react-router-dom/es/Redirect';

export class AccessRoom extends React.Component {
  render() {
    if (this.props.location) return <Redirect to={'/make-room'} />;

    const {roomId} = this.props.location.state;
    const link = `localhost:8080/room/${roomId}`;
    return (
      <AccessRoomStyled>
        {({classes}) => (
          <div className={classes.container}>
            <h1 className={classes.title}>Please connect at:</h1>
            <a className={classes.link} href={link}>
              {link}
            </a>
          </div>
        )}
      </AccessRoomStyled>
    );
  }
}
