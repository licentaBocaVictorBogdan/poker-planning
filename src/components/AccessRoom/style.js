import {createStyled} from '../../common/MaterialUi/createStyled';

export const AccessRoomStyled = createStyled({
  container: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
  },
  title: {
    fontSize: 36,
    marginLeft: 10,
    marginTop: 40,
  },
  link: {
    marginLeft: 10,
    marginTop: 25,
  },
});
