import * as React from 'react';
import LoggedNavBar from '../../LoggedNavBar/LoggedNavBar';
import {axiosInstance, getUsername} from '../../../config';
import {RoomCard} from '../../../common/RoomCard/RoomCard';
import Grid from '@material-ui/core/Grid';
import Chip from '@material-ui/core/Chip';

export class RoomsPage extends React.Component {
  state = {
    rooms: [],
    getRooms: true,
  };

  getUserRooms = () =>
    axiosInstance.get('/rooms').then(({data}) =>
      this.setState({
        rooms: data,
        getRooms: false,
      }),
    );

  render() {
    this.state.getRooms && this.getUserRooms();

    return (
      <>
        <LoggedNavBar leftString={getUsername()} />
        <div style={{margin: 25}}>
          <Grid container={true} spacing={40}>
            {this.state.rooms.length === 0 && (
              <Grid item={true} xs={12} md={12}>
                <div
                  style={{
                    display: 'flex',
                    justifyContent: 'center',
                  }}>
                  <Chip
                    color="default"
                    label={'You have no rooms created'}
                    variant="outlined"
                  />
                </div>
              </Grid>
            )}
            {this.state.rooms.map(room => (
              <Grid item={true} xs={12} md={4} key={room.roomId}>
                <RoomCard
                  title={room.name}
                  roomId={room.roomId}
                  description={room.description}
                  hours={room.finalVote}
                />
              </Grid>
            ))}
          </Grid>
        </div>
      </>
    );
  }
}
