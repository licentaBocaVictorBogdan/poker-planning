import * as React from 'react';
import LoggedNavBar from '../../LoggedNavBar/LoggedNavBar';
import {axiosInstance, getUserId, getUsername} from '../../../config';
import {ChatCard} from '../../../common/ChatCard/ChatCard';
import Grid from '@material-ui/core/Grid';
import {HoursVote} from '../../../common/HoursVote/HoursVote';
import {withRouter} from 'react-router-dom';
import {debounce} from 'throttle-debounce';

class ChatPage extends React.Component {
  state = {
    getArguments: true,
    prediction: '',
    personsArguments: [],
    disabled: false,
  };

  getArguments = () => {
    const {id} = this.props.match.params;

    axiosInstance.get(`/chat/${id}`).then(({data}) =>
      this.setState({
        personsArguments: data.personsDebate,
        prediction: data.finalVote
          ? data.finalVote
          : this.state.prediction === ''
          ? data.prediction
          : this.state.prediction !== data.prediction
          ? this.state.prediction
          : data.prediction,
        disabled: !!data.finalVote,
      }),
    );
  };

  fetchArguments = debounce(1000, false, this.getArguments);

  postArgument = argument => {
    const {id} = this.props.match.params;

    axiosInstance
      .post(`/chat/${id}`, {argument})
      .then(() => this.getArguments());
  };

  predictionChange = prediction => this.setState({prediction});

  finalVote = vote => {
    const {id} = this.props.match.params;

    axiosInstance
      .post(`/chat/${id}/final`, {finalVote: vote})
      .then(() => this.props.history.push('/home'));
  };

  render() {
    this.state.getArguments && this.fetchArguments();
    const userId = getUserId();
    const min = this.state.personsArguments[0];
    const max = this.state.personsArguments[1];

    return (
      <>
        <LoggedNavBar leftString={getUsername()} />
        <Grid container={true} spacing={0}>
          <Grid item={true} xs={12} md={5}>
            <ChatCard
              cardTitle={'The lowest'}
              cardContent={min && min.personArgument}
              needToArgument={min && userId === min.personId}
              handleSubmit={this.postArgument}
            />
          </Grid>
          <Grid item={true} xs={12} md={2}>
            <HoursVote
              buttonText={'Final vote'}
              personsVote={this.state.prediction}
              onChange={this.predictionChange}
              onSave={this.finalVote}
              disabled={this.state.disabled}
            />
          </Grid>
          <Grid item={true} xs={12} md={5}>
            <ChatCard
              cardTitle={'The biggest'}
              cardContent={max && max.personArgument}
              needToArgument={max && userId === max.personId}
              handleSubmit={this.postArgument}
            />
          </Grid>
        </Grid>
      </>
    );
  }
}

export default withRouter(ChatPage);
