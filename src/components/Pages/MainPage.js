import React from 'react';
import {NavBar} from '../../common/NavBar/NavBar';
import {Informations} from '../../common/Informations/Informations';
import MakeAccount from '../MakeAccount/MakeAccount';
import Login from '../Login/Login';
import Grid from '@material-ui/core/Grid';

export class MainPage extends React.Component {
  render() {
    return (
      <>
        <NavBar rightSide={<Login />} />
        <Grid container={true}>
          <Grid item={true} xs={12} md={6}>
            <Informations />
          </Grid>
          <Grid item={true} xs={12} md={6}>
            <MakeAccount />
          </Grid>
        </Grid>
      </>
    );
  }
}
