import React from 'react';
import {HomePage} from './HomePage/HomePage';
import {VotingPage} from './VotingPage/VotingPage';
import {PrivateRoute} from '../../routes/PrivateRoute';
import {Redirect} from 'react-router-dom';
import ChatPage from './ChatPage/ChatPage';
import {RoomsPage} from './RoomsPage/RoomsPage';

export default class LoggedInPage extends React.Component {
  render() {
    return (
      <>
        <PrivateRoute path={'/home'} exact={true} component={HomePage} />
        <PrivateRoute path={'/room/:id'} exact={true} component={VotingPage} />
        <PrivateRoute path={'/chat/:id'} exact={true} component={ChatPage} />
        <PrivateRoute path={'/rooms'} exact={true} component={RoomsPage} />
        <PrivateRoute
          path={'/'}
          exact={true}
          component={() => <Redirect to={'/home'} />}
        />
      </>
    );
  }
}
