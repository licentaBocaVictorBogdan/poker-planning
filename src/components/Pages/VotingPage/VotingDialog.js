import * as React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import {HoursVote} from '../../../common/HoursVote/HoursVote';

export class VotingDialog extends React.Component {
  handleSave = vote => this.props.handleSave(vote);

  render() {
    return (
      <div>
        <Dialog
          open={this.props.open}
          onClose={this.props.handleClose}
          fullScreen={window.screen.width < 960}>
          <DialogTitle>{'How much do you think will take?'}</DialogTitle>
          <DialogContent>
            <HoursVote onSave={this.handleSave} buttonText={'Vote'} />
          </DialogContent>
          <DialogActions>
            <Button onClick={this.props.handleClose} color="secondary">
              Close
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}
