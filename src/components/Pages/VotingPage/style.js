import {createStyled} from '../../../common/MaterialUi/createStyled';

export const VotingPageStyled = createStyled({
  root: {
    display: 'flex',
    justifyContent: 'space-between',
    margin: 25,
  },
  title: {
    fontSize: 24,
  },
  voteButton: {
    minWidth: 150,
  },
  description: {
    marginBottom: 25,
    marginRight: 25,
    marginLeft: 25,
    textAlign: 'justify',
    textIndent: 15,
  },
});
