import * as React from 'react';
import LoggedNavBar from '../../LoggedNavBar/LoggedNavBar';
import Button from '@material-ui/core/es/Button/Button';
import {axiosInstance} from '../../../config';
import {VotingPageStyled} from './style';
import {VotingDialog} from './VotingDialog';
import PersonsCards from './PersonsCards';

export class VotingPage extends React.Component {
  state = {
    getPageInfo: true,
    showVoteDialog: false,
  };

  getPageInfo = () => {
    const {id} = this.props.match.params;

    axiosInstance.get(`room/${id}`).then(({data}) =>
      this.setState({
        ...data,
        getPageInfo: false,
      }),
    );
  };

  ableJoinChat = able => this.setState({ableJoinChat: able});

  handleVote = () =>
    this.setState({showVoteDialog: !this.state.showVoteDialog});
  onVote = vote => {
    const {id} = this.props.match.params;

    axiosInstance
      .post(`vote/${id}`, {
        roomId: id,
        vote,
      })
      .then(() => this.handleVote());
  };

  redirectToChat = () => {
    const {id} = this.props.match.params;

    this.props.history.push(`/chat/${id}`);
  };

  render() {
    if (this.state.getPageInfo) this.getPageInfo();
    return (
      <>
        <LoggedNavBar leftString={this.state.name} />
        <VotingPageStyled>
          {({classes}) => (
            <>
              <div className={classes.root}>
                <div>
                  <h1 className={classes.title}>Task description</h1>
                </div>
                <div>
                  <Button
                    className={classes.voteButton}
                    onClick={this.handleVote}>
                    Vote
                  </Button>
                </div>
              </div>
              <p className={classes.description}>{this.state.description}</p>
              <PersonsCards ableToJoinChat={this.ableJoinChat} />
              <VotingDialog
                open={this.state.showVoteDialog}
                handleClose={this.handleVote}
                handleSave={this.onVote}
              />
            </>
          )}
        </VotingPageStyled>
      </>
    );
  }
}
