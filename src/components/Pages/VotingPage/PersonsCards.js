import * as React from 'react';
import Grid from '@material-ui/core/Grid';
import {VotantCard} from '../../../common/VotantCard/VotantCard';
import {axiosInstance, getUserId} from '../../../config';
import Button from '@material-ui/core/Button';
import {withRouter} from 'react-router-dom';
import {debounce} from 'throttle-debounce';

class PersonsCards extends React.Component {
  state = {
    personsList: [],
    getPersons: true,
  };

  getPersonsVotes = () => {
    const {id} = this.props.match.params;

    axiosInstance
      .get(`votingList/${id}`)
      .then(({data}) => this.setState({...data}));
  };

  fetchPersons = debounce(1000, false, this.getPersonsVotes);

  redirectToChat = () => {
    const {id} = this.props.match.params;

    this.props.history.push(`/chat/${id}`);
  };

  render() {
    if (this.state.getPersons) this.fetchPersons();
    const personCards = this.state.personsList.map(person => (
      <Grid key={person.personId} item={true} xs={6} md={2}>
        <VotantCard
          username={person.personName}
          voting={this.state.getPersons}
          vote={person.personVote}
        />
      </Grid>
    ));
    const ableToVote = this.state.personsList.find(
      person => person.personId === getUserId(),
    )
      ? !this.state.personsList
          .find(person => person.personId === getUserId())
          .hasOwnProperty('personVote')
      : true;

    return (
      <>
        <Grid container={true}>
          <Grid item={true} xs={12} md={12}>
            {!this.state.getPersons && !ableToVote && (
              <Grid item={true} xs={12} md={6}>
                <Button fullWidth={true} onClick={this.redirectToChat}>
                  Go to chat room
                </Button>
              </Grid>
            )}
          </Grid>
          {personCards}
        </Grid>
      </>
    );
  }
}

export default withRouter(PersonsCards);
