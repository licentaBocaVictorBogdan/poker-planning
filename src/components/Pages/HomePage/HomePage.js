import * as React from 'react';
import {DefaultLayout} from '../../../style/defaultPageLayout';
import {PPInput} from '../../../style/inputs';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import {MakeTask} from '../../../common/MakeTask/MakeTask';
import {Informations} from '../../../common/Informations/Informations';
import {axiosInstance, getUsername} from '../../../config';
import LoggedNavBar from '../../LoggedNavBar/LoggedNavBar';
import Grid from '@material-ui/core/Grid';

export class HomePage extends React.Component {
  state = {
    username: getUsername(),
    roomId: '',
  };

  handleRoomId = event => this.setState({roomId: event.target.value});

  joinRoom = event => {
    event.preventDefault();

    axiosInstance
      .post(`/room/${this.state.roomId}`)
      .catch(err => console.log(err))
      .then(() => this.props.history.push(`/room/${this.state.roomId}`));
  };

  render() {
    return (
      <>
        <LoggedNavBar leftString={this.state.username} />

        <DefaultLayout>
          {({classes}) => (
            <div className={classes.container}>
              <Grid container={true}>
                <Grid item={true} xs={12} md={6}>
                  <Grid item={true} xs={12} md={12}>
                    <h1>Join to a room</h1>
                    <form
                      className={classes.containerSplit}
                      onSubmit={this.joinRoom}>
                      <PPInput>
                        {({classes: inputClasses}) => (
                          <TextField
                            className={inputClasses.input}
                            label="Please enter room id"
                            value={this.state.roomId}
                            onChange={this.handleRoomId}
                            margin="normal"
                            variant="outlined"
                            required={true}
                          />
                        )}
                      </PPInput>
                      <Button className={classes.submitButton} type="submit">
                        Join ROOM
                      </Button>
                    </form>
                  </Grid>
                  <Grid item={true} xs={12} md={12}>
                    <MakeTask />
                  </Grid>
                </Grid>
                <Grid item={true} xs={12} md={6}>
                  <Informations />
                </Grid>
              </Grid>
            </div>
          )}
        </DefaultLayout>
      </>
    );
  }
}
