import * as React from 'react';
import HomePage from '../components/Pages/LoggedInPage';
import {MainPage} from '../components/Pages/MainPage';
import {checkAuth} from './checkAuth';

export const AuthButton = () =>
  checkAuth.isAuthenticated ? <HomePage /> : <MainPage />;
