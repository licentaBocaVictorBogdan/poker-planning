import {Redirect, Route} from 'react-router-dom';
import React from 'react';
import {checkAuth} from './checkAuth';
import {axiosInstance, localStorageTokenName} from '../config';

export function PrivateRoute({component: Component, ...rest}) {
  if (checkAuth.isAuthenticated) {
    axiosInstance.defaults.headers.common[
      'Authorization'
    ] = `Bearer ${JSON.parse(localStorage.getItem(localStorageTokenName)) &&
      JSON.parse(localStorage.getItem(localStorageTokenName)).token}`;
  }
  return (
    <Route
      {...rest}
      render={props =>
        checkAuth.isAuthenticated ? (
          <Component {...props} />
        ) : (
          <Redirect to={{pathname: '/', state: {from: props.location}}} />
        )
      }
    />
  );
}
