import {localStorageTokenName} from '../config';

export const checkAuth = {
  isAuthenticated: !!localStorage.getItem(localStorageTokenName),
  authenticate(lsObject, cb) {
    this.isAuthenticated = true;
    localStorage.setItem(localStorageTokenName, JSON.stringify(lsObject));
    cb();
  },
  signout(cb) {
    this.isAuthenticated = false;
    localStorage.removeItem(localStorageTokenName);
    cb();
  },
};
