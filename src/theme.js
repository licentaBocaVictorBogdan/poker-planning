import {createMuiTheme} from '@material-ui/core';

export const theme = createMuiTheme({
  palette: {
    background: {
      default: '#fff176',
      paper: '#fff176',
    },
    primary: {
      contrastText: '#ffffff',
      dark: '#03a9f4',
      light: '#0377b0',
      main: '#0377b0',
    },
    secondary: {
      contrastText: '#ffffff',
      dark: '#ff5722',
      light: '#ef5622',
      main: '#ffffff',
    },
  },
  props: {
    MuiButton: {
      style: {backgroundColor: '#03a9f4'},
    },
  },
});
