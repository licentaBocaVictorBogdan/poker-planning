import {createStyled} from '../common/MaterialUi/createStyled';

export const DefaultLayout = createStyled({
  container: {
    display: 'flex',
    justifyContent: 'space-around',
    margin: 25,
  },
  containerSplit: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  leftSide: {
    flex: 1,
  },
  rightSide: {
    flex: 1,
  },
});
