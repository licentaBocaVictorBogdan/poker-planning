import {createStyled} from '../common/MaterialUi/createStyled';

export const PPInput = createStyled({
  input: {
    height: 36,
    margin: '0 15px 0 0',
    '& label': {
      "&[data-shrink='false']": {
        transform: 'translate(10px, 14px) scale(1);',
      },
      "&[data-shrink='true']": {
        transform: 'translate(14px, -4px) scale(0.75) !important;',
        fontWeight: 500,
      },
    },
    '& input': {
      padding: '8.5px 14px',
    },
  },
});
